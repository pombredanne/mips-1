include Makefile.am


all: $(OBJS)
	$(CC) $(CFLAGS) $(LIBS) $(INCLUDES) $(MAIN) $(OBJS) -o $(BIN)


%.o: %.cc
	$(CC) $(LIBS) $(INCLUDES) $(CFLAGS) -c $< -o $@


clean:
	rm -rf $(OBJS)


distclean: clean
	rm -f $(BIN) output
	rm -rf doc/latex doc/html
	rm -f doc/Doxyfile.bak
	rm -rf $(TARNAME) $(TARNAME).tar.bz2


doc: doc/Doxyfile
	cd doc && doxygen Doxyfile && make -C latex


dist: distclean
	mkdir $(TARNAME)
	cp -r $(TOCP) $(TARNAME)
	tar --exclude-vcs --exclude=tar -cjf $(TARNAME).tar.bz2 $(TARNAME)
	rm -rf $(TARNAME)
