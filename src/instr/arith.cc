#include <instr/arith.hh>


void
Arith::add(uint32_t ins)
{
    if (DBG_INS(g_debug))
        fprintf(stderr, "add $%u, $%u, $%u\n", GRD(ins), GRS(ins), GRT(ins));

    g_reg->set(GRD(ins), g_reg->get(GRT(ins)) + g_reg->get(GRS(ins)));
}


void
Arith::addu(uint32_t ins)
{
    if (DBG_INS(g_debug))
        fprintf(stderr, "addu $%u, $%u, $%u\n", GRD(ins), GRS(ins), GRT(ins));

    g_reg->set(GRD(ins), g_reg->get(GRT(ins)) + g_reg->get(GRS(ins)));
}


void
Arith::addi(uint32_t ins)
{
    if (DBG_INS(g_debug))
        fprintf(stderr, "addi $%u, $%u, 0x%X\n", GRT(ins), GRS(ins), GRT(ins));

    g_reg->set(GRT(ins), g_reg->get(GRS(ins)) + (int32_t)(int16_t)GIM(ins));
}


void
Arith::addiu(uint32_t ins)
{
    if (DBG_INS(g_debug))
        fprintf(stderr, "addiu $%u, $%u, 0x%X\n",
            GRT(ins), GRS(ins), GIM(ins));

    g_reg->set(GRT(ins), g_reg->get(GRS(ins)) + (int32_t)(int16_t)GIM(ins));
}


void
Arith::div(uint32_t ins)
{
    if (DBG_INS(g_debug))
        fprintf(stderr, "div $%u, $%u\n", GRS(ins), GRT(ins));

    g_reg->setlo(g_reg->get(GRS(ins)) / g_reg->get(GRT(ins)));
    g_reg->sethi(g_reg->get(GRS(ins)) % g_reg->get(GRT(ins)));
}


void
Arith::divu(uint32_t ins)
{
    if (DBG_INS(g_debug))
        fprintf(stderr, "divu $%u, $%u\n", GRS(ins), GRT(ins));

    g_reg->setlo(g_reg->get(GRS(ins)) / g_reg->get(GRT(ins)));
    g_reg->sethi(g_reg->get(GRS(ins)) % g_reg->get(GRT(ins)));
}


void
Arith::mult(uint32_t ins)
{
    if (DBG_INS(g_debug))
        fprintf(stderr, "mult $%u, $%u\n", GRS(ins), GRT(ins));

    int64_t result(g_reg->get(GRS(ins)) * g_reg->get(GRT(ins)));
    g_reg->setlo(result & 0xFFFF);
    g_reg->sethi(result >> 16);
}


void
Arith::multu(uint32_t ins)
{
    if (DBG_INS(g_debug))
        fprintf(stderr, "multu $%u, $%u\n", GRS(ins), GRT(ins));

    uint64_t result(g_reg->get(GRS(ins)) * g_reg->get(GRT(ins)));
    g_reg->setlo(result & 0xFFFF);
    g_reg->sethi(result >> 16);
}


void
Arith::sub(uint32_t ins)
{
    if (DBG_INS(g_debug))
        fprintf(stderr, "sub $%u, $%u, $%u\n", GRD(ins), GRS(ins), GRT(ins));

    g_reg->set(GRD(ins), g_reg->get(GRS(ins)) - g_reg->get(GRT(ins)));
}


void
Arith::subu(uint32_t ins)
{
    if (DBG_INS(g_debug))
        fprintf(stderr, "subu $%u, $%u, $%u\n", GRD(ins), GRS(ins), GRT(ins));

    g_reg->set(GRD(ins), g_reg->get(GRS(ins)) - g_reg->get(GRT(ins)));
}
