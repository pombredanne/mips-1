#ifndef LOGIC_HH_
# define LOGIC_HH_

# include <boost/cstdint.hpp>

# include <instr/bitwise.hh>
# include <env/registers.hh>
# include <debug.hh>


namespace Logic
{
    void and_instr(uint32_t ins);
    void andi(uint32_t ins);

    void nor(uint32_t ins);
    void or_instr(uint32_t ins);
    void ori(uint32_t ins);

    void slt(uint32_t ins);
    void sltu(uint32_t ins);
    void slti(uint32_t ins);
    void sltiu(uint32_t ins);

    void xor_instr(uint32_t ins);
    void xori(uint32_t ins);
} // Logic


#endif /* !LOGIC_HH_ */
