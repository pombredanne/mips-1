#include <instr/store.hh>


void
Store::lb(uint32_t ins)
{
    if (DBG_INS(g_debug))
        fprintf(stderr, "lb $%u, 0x%X($%u)\n", GRT(ins), GRS(ins), GIM(ins));

    g_reg->set(GRT(ins),
        (int32_t)(int8_t)*g_mem->get((int32_t)(int16_t)GIM(ins) +
        g_reg->get(GRS(ins))));
}


void
Store::lbu(uint32_t ins)
{
    if (DBG_INS(g_debug))
        fprintf(stderr, "lbu $%u, 0x%X($%u)\n", GRT(ins), GIM(ins), GRS(ins));

    g_reg->set(GRT(ins),
        (uint32_t)(uint8_t)*g_mem->get((int32_t)(int16_t)GIM(ins) +
        g_reg->get(GRS(ins))));
}


void
Store::lh(uint32_t ins)
{
    if (DBG_INS(g_debug))
        fprintf(stderr, "lh $%u, 0x%X($%u)\n", GRT(ins), GIM(ins), GRS(ins));

    char* address(g_mem->get((int32_t)(int16_t)GIM(ins) +
        g_reg->get(GRS(ins))));

    int32_t result((int16_t)((*(address) << 8) + (uint8_t)*(address + 1)));
    g_reg->set(GRT(ins), result);
}


void
Store::lhu(uint32_t ins)
{
    if (DBG_INS(g_debug))
        fprintf(stderr, "lhu $%u, 0x%X($%u)\n", GRT(ins), GIM(ins), GRS(ins));

    char* address(g_mem->get((int32_t)(int16_t)GIM(ins) +
        g_reg->get(GRS(ins))));

    uint32_t result((uint16_t)((*(address) << 8) + (uint8_t)*(address + 1)));
    g_reg->set(GRT(ins), result);
}


void
Store::lw(uint32_t ins)
{
    if (DBG_INS(g_debug))
        fprintf(stderr, "lw $%u, 0x%X($%u)\n", GRT(ins), GIM(ins), GRS(ins));

    char* address(g_mem->get((int32_t)(int16_t)GIM(ins) +
        g_reg->get(GRS(ins))));

    g_reg->set(GRT(ins), TO_INT(address));
}


void
Store::sb(uint32_t ins)
{
    if (DBG_INS(g_debug))
        fprintf(stderr, "sb $%u, 0x%X($%u)\n", GRT(ins), GIM(ins), GRS(ins));

    *g_mem->get((int32_t)(int16_t)GIM(ins) + g_reg->get(GRS(ins))) =
        g_reg->get(GRT(ins)) & 0xFF;
}


void
Store::sh(uint32_t ins)
{
    if (DBG_INS(g_debug))
        fprintf(stderr, "sw $%u, 0x%X($%u)\n", GRT(ins), GIM(ins), GRS(ins));

    char* address(g_mem->get((int32_t)(int16_t)GIM(ins) +
        g_reg->get(GRS(ins))));

    uint32_t content(g_reg->get(GRT(ins)));
    *(address) = (content >> 8) & 0xFF;
    *(address + 1) = (content >> 16) & 0xFF;
}


void
Store::sw(uint32_t ins)
{
    if (DBG_INS(g_debug))
        fprintf(stderr, "sw $%u, 0x%X($%u)\n", GRT(ins), GIM(ins), GRS(ins));

    char* address(g_mem->get((int32_t)(int16_t)GIM(ins) +
        g_reg->get(GRS(ins))));

    uint32_t content(g_reg->get(GRT(ins)));
    *(address) = (content >> 24) & 0xFF;
    *(address + 1) = (content >> 16) & 0xFF;
    *(address + 2) = (content >> 8) & 0xFF;
    *(address + 3) = content & 0xFF;
}


void
Store::lui(uint32_t ins)
{
    if (DBG_INS(g_debug))
        fprintf(stderr, "lui $%u, 0x%X\n", GRT(ins), GIM(ins));

    g_reg->set(GRT(ins), (int16_t)GIM(ins) << 16);
}


void
Store::mflo(uint32_t ins)
{
    if (DBG_INS(g_debug))
        fprintf(stderr, "mflo $%u\n", GRD(ins));

    g_reg->set(GRD(ins), g_reg->getlo());
}


void
Store::mfhi(uint32_t ins)
{
    if (DBG_INS(g_debug))
        fprintf(stderr, "mfhi $%u\n", GRD(ins));

    g_reg->set(GRD(ins), g_reg->gethi());
}


void
Store::mtlo(uint32_t ins)
{
    if (DBG_INS(g_debug))
        fprintf(stderr, "mtlo $%u\n", GRS(ins));

    g_reg->setlo(g_reg->get(GRS(ins)));
}


void
Store::mthi(uint32_t ins)
{
    if (DBG_INS(g_debug))
        fprintf(stderr, "mthi $%u\n", GRS(ins));

    g_reg->sethi(g_reg->get(GRS(ins)));
}
