#ifndef BRANCH_HH_
# define BRANCH_HH_

# include <iostream>
# include <boost/cstdint.hpp>

# include <instr/bitwise.hh>
# include <env/registers.hh>
# include <debug.hh>


typedef enum
{
    BGEZ = 0x01,
    BGEZAL = 0x11,
    BLTZ = 0x00,
    BLTZAL = 0x10,
} e_regimm_branch;


namespace Branch
{
    void jalr(uint32_t ins);
    void jal(uint32_t ins);
    void jr(uint32_t ins);
    void j(uint32_t ins);

    void beq(uint32_t ins);
    void bne(uint32_t ins);

    void regimm(uint32_t ins);

    void bgtz(uint32_t ins);
    void blez(uint32_t ins);
} // Branch


#endif /* !BRANCH_HH_ */
