#ifndef BITWISE_OPERATIONS_HH_
# define BITWISE_OPERATIONS_HH_

/*
** bitwise operations on the instruction. the endianess is reversed in
** comparison with the instructions displayed in the manual.
**
** for example:
** read 0x4000000C in the file, whereas the instruction jal 0x100 is the
** reverse, 0x0C000040. we however extract the data from 0x4000000C.
**
** this "trick" allows us not to bother about the swap between endianess in the
** registers/processor and memory/file. we just take it little-endian.
*/
# define GET_OPCODE(ins)    ((ins >> 2) & 0x3F)
# define GET_FC(ins)        ((ins >> 24) & 0x3F)
# define GET_RS(ins)        (((ins << 3) & 0x18) + ((ins >> 13) & 0x7))
# define GET_RT(ins)        ((ins >> 8) & 0x1F)
# define GET_RD(ins)        ((ins >> 19) & 0x1F)
# define GET_SA(ins)        (((ins >> 30) & 0x3) + ((ins >> 14) & 0x1C))
# define GET_IM(ins)        (((ins >> 8) & 0xFF00) + ((ins >> 24) & 0xFF))
# define GET_AD(ins)        (GET_IM(ins) + ((ins << 8) & 0xFF0000) +          \
                            ((ins << 24) & 0x3000000))

// shortcuts
# define GRS(ins)           GET_RS(ins)
# define GRT(ins)           GET_RT(ins)
# define GRD(ins)           GET_RD(ins)
# define GSA(ins)           GET_SA(ins)
# define GIM(ins)           GET_IM(ins)
# define GAD(ins)           GET_AD(ins)

// retrieve uint32_t from memory
# define TO_INT(loc)        ((((uint32_t)*(loc) & 0xFF) << 24) |              \
                            (((uint32_t)*(loc + 1) & 0xFF) << 16) |           \
                            (((uint32_t)*(loc + 2) & 0xFF) << 8) |            \
                            (((uint32_t)*(loc + 3) & 0xFF)))

#endif /* !BITWISE_OPERATIONS_HH_ */
