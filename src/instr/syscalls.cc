#include <instr/syscalls.hh>


static void
print_int_sys()
{
    std::cout << g_reg->get(A0) << std::flush;
}


static void
print_str_sys()
{
    std::cout << g_mem->get(g_reg->get(A0)) << std::flush;
}


static void
read_int_sys()
{
    int hold;
    std::cin >> hold;
    g_reg->set(V0, hold);
}


static void
read_str_sys()
{
    std::cin.read(g_mem->get(g_reg->get(A0)), g_reg->get(A1));
}


static void
exit_sys()
{
    throw Exit_Signal();
}


static void
print_chr_sys()
{
    std::cout << (char)g_reg->get(A0) << std::flush;
}


static void
read_chr_sys()
{
    char hold;
    std::cin.read(&hold, 1);
    g_reg->set(V0, hold);
}


static void
open_sys()
{
    g_reg->set(V0, open(g_mem->get(g_reg->get(A0)),
        g_reg->get(A1) == 0 ? O_RDONLY : O_WRONLY));
}


static void
read_sys()
{
    g_reg->set(V0, read(g_reg->get(A0), g_mem->get(g_reg->get(A1)),
        g_reg->get(A2)));
}


static void
write_sys()
{
    g_reg->set(V0, write(g_reg->get(A0), g_mem->get(g_reg->get(A1)),
        g_reg->get(A2)));
}


static void
close_sys()
{
    close(g_reg->get(A0));
}


static void
exit2_sys()
{
    throw Exit_Signal(g_reg->get(A0));
}


static const uint32_t g_syscall_nbr = 18;
static const f_syscall g_syscalls_tbl[g_syscall_nbr] =
{
    0x0,
    print_int_sys,
    0x0,
    0x0,
    print_str_sys,
    read_int_sys,
    0x0,
    0x0,
    read_str_sys,
    0x0,
    exit_sys,
    print_chr_sys,
    read_chr_sys,
    open_sys,
    read_sys,
    write_sys,
    close_sys,
    exit2_sys
};


void
syscall(uint32_t ins)
{
    uint32_t systype(g_reg->get(V0));
    UNUSED(ins);

    if (DBG_INS(g_debug))
        fprintf(stderr, "syscall no. %u\n", systype);

    f_syscall toexec(0x0);
    if (systype >= g_syscall_nbr || (toexec = g_syscalls_tbl[systype]) == 0x0)
        throw std::logic_error(BAD_SYSCALL);
    else
        toexec();
}


Exit_Signal::Exit_Signal(int newcode)
{
    this->code = newcode;
}


int
Exit_Signal::returncode() const throw()
{
    return this->code;
}
