#ifndef SYSCALLS_HH_
# define SYSCALLS_HH_

# include <iostream>
# include <stdexcept>
# include <boost/cstdint.hpp>

# include <unistd.h>
# include <sys/stat.h>
# include <fcntl.h>

# include <env/memory.hh>
# include <env/registers.hh>
# include <debug.hh>

# define BAD_SYSCALL    "syscall not found"


typedef void (*f_syscall)(void);


class Exit_Signal : public std::exception
{
    public:
    Exit_Signal(int newcode = 0);
    int returncode() const throw();

    private:
    int code;
};


void syscall(uint32_t ins);


#endif /* !SYSCALLS_HH_ */
