#ifndef STORE_HH_
# define STORE_HH_

# include <stdexcept>
# include <boost/cstdint.hpp>

# include <env/memory.hh>
# include <env/registers.hh>
# include <instr/bitwise.hh>
# include <debug.hh>


namespace Store
{
    void lb(uint32_t ins);
    void lbu(uint32_t ins);

    void lh(uint32_t ins);
    void lhu(uint32_t ins);

    void lw(uint32_t ins);
    void lwl(uint32_t ins);
    void lwr(uint32_t ins);

    void sb(uint32_t ins);
    void sh(uint32_t ins);

    void sw(uint32_t ins);
    void swl(uint32_t ins);
    void swr(uint32_t ins);

    void lui(uint32_t ins);

    void mflo(uint32_t ins);
    void mfhi(uint32_t ins);
    void mtlo(uint32_t ins);
    void mthi(uint32_t ins);
} // Store


#endif /* !STORE_HH_ */
