#ifndef DEBUG_HH_
# define DEBUG_HH_

// indicate, considering the 'dbgvalue' wether or not to print debug/registers
# define DBG_REG(dbgvalue)  ((dbgvalue >> 1) == 1)
# define DBG_INS(dbgvalue)  ((dbgvalue & 1) == 1)

# define MYMIPS_PROMPT      "[my_mips] "
# define DBG_MSG            MYMIPS_PROMPT "Executing pc = "


extern int g_debug;


#endif /* !DEBUG_HH_ */
