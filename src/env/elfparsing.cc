#include <env/elfparsing.hh>


ELF::ELF(std::ifstream& input)
{
    input.seekg(0);

    // load the elf header
    this->header = new Elf32_Ehdr();
    input.read((char*)this->header, sizeof(Elf32_Ehdr));

    // handle bad input files
    if (this->header->e_machine != EM_MIPS ||
        this->header->e_phnum == 0 ||
        this->header->e_phentsize != sizeof (Elf32_Phdr))
    {
        delete this->header;
        throw std::runtime_error(ELF_ERR);
    }

    //  programheader for future segments loading
    this->ph_entry = new Elf32_Phdr();

    // save offset
    input.seekg(this->header->e_phoff);
    this->cur_offset = input.tellg();
}


ELF::~ELF()
{
    delete this->header;
    delete this->ph_entry;
}


bool
ELF::iself(std::ifstream& input)
{
    char magic[4];

    // retrieve magic nbr
    input.read(magic, 4);

    input.seekg(0);
    return magic[0] == ELFMAG0 &&
        magic[1] == ELFMAG1 &&
        magic[2] == ELFMAG2 &&
        magic[3] == ELFMAG3;
}


// yield information on the next segment to load from the file
// returns false if there are no more to be read.
bool
ELF::next_segment(uint32_t* virtual_addr, uint32_t* real_addr, uint32_t* size,
    std::ifstream& input)
{
    // number of program header entries read
    static uint32_t ph_entry(0);
    input.seekg(this->cur_offset);

    // no (more) program headers to be read
    if (ph_entry > this->header->e_phnum)
        return false;

    // go to the next loadable program header, and load it in ph_entry
    do {
        if (input.eof())
            throw std::runtime_error(ELF_ERR);

        input.read((char*)this->ph_entry, sizeof(Elf32_Phdr));
    }
    while (this->ph_entry->p_type != PT_LOAD &&
        ph_entry++ < this->header->e_phnum);

    this->cur_offset = input.tellg();

    if (this->ph_entry->p_type == PT_LOAD)
    {
        // loadable segment found
        *virtual_addr = this->ph_entry->p_vaddr;
        *real_addr = this->ph_entry->p_offset;
        *size = this->ph_entry->p_memsz;
        return true;
    }

    // no (more) program headers to be read
    return false;
}
