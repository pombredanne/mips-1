#ifndef MEMORY_HH_
# define MEMORY_HH_

# include <fstream>
# include <stdexcept>
# include <boost/cstdint.hpp>

# include <debug.hh>
# include <instr/bitwise.hh>
# include <env/elfparsing.hh>

# define UNUSED(expr)   do { (void)(expr); } while (0)
# define SEGFAULT       "segmentation fault"
# define MEM_OVERFLOW   "memory overflow"


class Memory
{
    public:
    Memory(const char* binname);
    ~Memory();

    char* get(const uint32_t address) const;

    private:
    char* grid;

    void load_segment(uint32_t virtual_addr, uint32_t real_addr, uint32_t size,
        std::ifstream& input);
};


extern const uint32_t g_mem_capacity;
extern const uint32_t g_mem_bot;

extern Memory* g_mem;


#endif /* !MEMORY_HH_ */
