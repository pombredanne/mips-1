#include <env/memory.hh>


const uint32_t g_mem_capacity = 1 << 24;
const uint32_t g_mem_bot = (g_mem_capacity - 1);

Memory* g_mem = 0x0;


// create the memory object and load the file in there
Memory::Memory(const char* binname)
{
    std::ifstream input(binname, std::ifstream::in);
    uint32_t seg_size(0);

    this->grid = new char[g_mem_capacity];

    try {
        if (ELF::iself(input))
        {
            uint32_t virt_addr(0);
            uint32_t real_addr(0);
            ELF file(input);

            // load all loadable segments in memory at their virtual address
            while (file.next_segment(&virt_addr, &real_addr, &seg_size, input))
                this->load_segment(virt_addr, real_addr, seg_size, input);
        }
        else
        {
            // stripped file with only .text segment
            // check length
            input.seekg(0, std::ios::end);
            seg_size = input.tellg();
            input.seekg(0);

            this->load_segment(0x0, 0, seg_size, input);
        }
    }
    catch (std::runtime_error& e)
    {
        delete[] this->grid;
        throw e;
    }
    catch (std::out_of_range& e)
    {
        delete[] this->grid;
        throw e;
    }
}


Memory::~Memory()
{
    delete[] this->grid;
}


// returns a pointer to the real memory behind "address."
char*
Memory::get(const uint32_t address) const
{
    if (address >= g_mem_capacity)
        throw std::out_of_range(SEGFAULT);

    return this->grid + address;
}


// load a file segment into memory
void
Memory::load_segment(uint32_t virtual_addr, uint32_t real_addr, uint32_t size,
    std::ifstream& input)
{
    if (virtual_addr + size >= g_mem_capacity)
        throw std::out_of_range(MEM_OVERFLOW);

    input.seekg(real_addr);
    input.read((char*)(this->grid + virtual_addr), size);
}
