#include <env/registers.hh>


const uint32_t g_reg_nbr = 32;
Registers* g_reg = 0x0;


Registers::Registers()
{
    this->grid = new int[g_reg_nbr];

    for (uint32_t i = 0; i < g_reg_nbr; i++)
        this->grid[i] = 0;

    // init special registers
    this->grid[SP] = this->grid[S8] = g_mem_bot;
    this->pc = 0x0;
}


Registers::~Registers()
{
    delete[] this->grid;
}


uint32_t
Registers::getpc() const
{
    return this->pc;
}


void
Registers::setpc(uint32_t value)
{
    if (value >= g_mem_capacity)
        throw std::out_of_range(SEGFAULT);

    this->pc = value;
}


void
Registers::incpc()
{
    if ((uint32_t)this->pc >= g_mem_capacity - 4)
        throw std::out_of_range(SEGFAULT);

    this->pc += 4;
}


int32_t
Registers::gethi() const
{
    return this->hi;
}


void
Registers::sethi(int value)
{
    this->hi = value;
}


int32_t
Registers::getlo() const
{
    return this->lo;
}


void
Registers::setlo(int value)
{
    this->lo = value;
}


std::ostream&
operator<<(std::ostream& out, const Registers& top)
{
    bool first = true;

    out << std::endl;

    for (uint32_t i = 0; i < g_reg_nbr; i++)
    {
        if (!first)
            out << std::endl;

        out << MYMIPS_PROMPT;
        out << "$" << std::setw(2) << std::setfill('0') << std::dec << i
            << " = 0x" << std::setw(8) << std::hex << top.grid[i];
        first = false;
    }

    return out << std::endl;
}
