template<class T>
int
Registers::get(T id) const
{
    if ((e_regid)id >= (e_regid)g_reg_nbr)
        throw std::out_of_range(SEGFAULT);

    return this->grid[id];
}


template<class T>
void
Registers::set(T id, int value)
{
    if ((e_regid)id >= (e_regid)g_reg_nbr)
        throw std::out_of_range(SEGFAULT);
    else if (id != 0)
        this->grid[id] = value;
}
